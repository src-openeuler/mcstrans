Name:           mcstrans
Version:        3.5
Release:        4
Summary:        SELinux Translation Daemon
License:        GPL2
URL:            https://github.com/SELinuxProject/selinux/wiki
Source:         https://github.com/SELinuxProject/selinux/releases/download/%{version}/%{name}-%{version}.tar.gz

Patch1:    backport-mcstrans-check-memory-allocations.patch
Patch2:    backport-mcstrans-free-constraint-in-error-branch.patch

BuildRequires:  gcc systemd-units make
BuildRequires:  libselinux-devel >= %{version}
BuildRequires:  libcap-devel pcre2-devel libsepol-devel >= %{version} libsepol-static >= %{version}
Requires:       pcre2
Requires(pre):  systemd
Requires(post): systemd
Provides:       libsetrans = %{version}-%{release}
Obsoletes:      libsetrans < %{version}-%{release}
Provides:       setransd

%description
Security-enhanced Linux is a feature of the Linux® kernel and a number
of utilities with enhanced security functionality designed to add
mandatory access controls to Linux.  The Security-enhanced Linux
kernel contains new architectural components originally developed to
improve the security of the Flask operating system. These
architectural components provide general support for the enforcement
of many kinds of mandatory access control policies, including those
based on the concepts of Type Enforcement®, Role-based Access
Control, and Multi-level Security.

mcstrans provides an translation daemon to translate SELinux categories
from internal representations to user defined representation.

%package help
Summary:        Mcstrans help document

%description help
mcstrans-help include help files for man page

%prep
%autosetup -n %{name}-%{version} -p1

%build
%set_build_flags

make LIBDIR="%{_libdir}" %{?_smp_mflags}

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/%{_lib}
mkdir -p %{buildroot}/%{_libdir}
mkdir -p %{buildroot}%{_usr}/share/mcstrans
mkdir -p %{buildroot}%{_sysconfdir}/selinux/mls/setrans.d

make DESTDIR="%{buildroot}" LIBDIR="%{_libdir}" SHLIBDIR="%{_lib}" SBINDIR="%{_sbindir}" install
rm -f %{buildroot}%{_libdir}/*.a
cp -r share/* %{buildroot}%{_usr}/share/mcstrans/
mkdir -p %{buildroot}%{_unitdir}
ln -s %{_unitdir}/mcstrans.service %{buildroot}/%{_unitdir}/mcstransd.service
rm -rf %{buildroot}/%{_sysconfdir}/rc.d/init.d/mcstrans

%post
%systemd_post mcstransd.service

%preun
%systemd_preun mcstransd.service

%postun
%systemd_postun mcstransd.service

%files
/usr/sbin/mcstransd
%{_unitdir}/mcstrans.service
%{_unitdir}/mcstransd.service
%dir %{_sysconfdir}/selinux/mls/setrans.d
%dir %{_usr}/share/mcstrans
%defattr(0644,root,root,0755)
%dir %{_usr}/share/mcstrans/util
%dir %{_usr}/share/mcstrans/examples
%{_usr}/share/mcstrans/examples/*
%defattr(0755,root,root,0755)
%{_usr}/share/mcstrans/util/*

%files help
%{_mandir}/man5/*.5.gz
%{_mandir}/man8/*.8.gz
%{_mandir}/ru/man5/*.5.gz
%{_mandir}/ru/man8/*.8.gz

%changelog
* Wed Feb 19 2025 yixiangzhike <yixiangzhike007@163.com> - 3.5-4
- use macro autosetup instead of patchN for merging patch file

* Wed Oct 23 2024 yixiangzhike <yixiangzhike007@163.com> - 3.5-3
- backport upstream patch to free constraint in error branch

* Mon Jul 8 2024 yixiangzhike <yixiangzhike007@163.com> - 3.5-2
- backport upstream patch to avoid NULL dereferences

* Thu Jul 20 2023 yixiangzhike <yixiangzhike007@163.com> - 3.5-1
- update to 3.5

* Wed Feb 1 2023 yixiangzhike <yixiangzhike007@163.com> - 3.4-1
- update to 3.4

* Sun May 15 2022 yixiangzhike <yixiangzhike007@163.com> - 3.3-2
- port to new PCRE2 from PCRE

* Mon Dec 13 2021 yixiangzhike <yixiangzhike007@163.com> - 3.3-1
- update to 3.3

* Thu Jul 23 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.1-1
- update to 3.1

* Thu Feb 13 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.3.4-17
- add BuildRequires: make

* Wed Dec 25 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.3.4-16
- Add missing URL

* Fri Sep 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.3.4-15
- Adjust requires

* Wed Jul 18 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.3.4-14
- Package init
